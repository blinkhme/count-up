'use strict';

/**
 * @ngdoc service
 * @name countUpApp.greet
 * @description
 * # greet
 * Service to return greeting based on time
 */
angular.module('countUpApp')
  .filter('greet', function() {
    return function(input) {
      if (input < 12) {
        return 'Good Morning';
      } else if (input >= 12 && input <= 17) {
        return 'Good Afternoon';
      } else if (input > 17 && input <= 24) {
        return 'Good Evening';
      } else {
        return "I'm not sure what time it is!";
      }
    };
  });