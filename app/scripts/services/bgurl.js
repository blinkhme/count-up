'use strict';

/**
 * @ngdoc service
 * @name countUpApp.bgurl
 * @description
 * # get the background image based on the current city's weather
 * Factory in the countUpApp.
 */
angular.module('countUpApp')
  .factory('bgurl', function () {
    var bg = Math.floor((Math.random() * 10) + 1);
    return '/images/bg' + bg + '.jpg';
  });
