'use strict';

/**
 * @ngdoc overview
 * @name countUpApp
 * @description
 * # countUpApp
 *
 * Main module of the application.
 */
angular
  .module('countUpApp', [
    'ngRoute',
    '720kb.background'
  ])
  .config(function($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
