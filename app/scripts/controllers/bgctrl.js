'use strict';

/**
 * @ngdoc function
 * @name countUpApp.controller:BgCtrl
 * @description
 * # Background controller
 * Controller of the countUpApp
 */
function BgCtrl($scope, bgurl) {
    $scope.bg = bgurl;
}