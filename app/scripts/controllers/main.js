'use strict';

/**
 * @ngdoc function
 * @name countUpApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the countUpApp
 */
angular.module('countUpApp')
  .controller('MainCtrl', function ($scope, $timeout) {
    $scope.timeNow = new Date();
    $scope.username = 'Alex';
    $scope.counter = 0;
    $scope.limit = 10;
    $scope.show_selected = false;

    var stopped;

    // count up to 10 and repeat
    $scope.countup = function() {
      stopped = $timeout(function() {
        if ($scope.counter == $scope.limit) {
          $scope.counter = 0;  
        }
        $scope.counter++; 
        $scope.countup();
      }, 1000);
    };

    // set the current count and reset
    $scope.stop = function(){
      $scope.stopped_num = $scope.counter;
      $scope.show_selected = true;
      $scope.counter = 0;
    } 
  });
