CountUp App
Author: Kean Hui Chuah

An angular timer app that counts from 0 to 10 infinitely. Pressing the button shows the current timer value.

- Display current time
- Personalised greeting depending on time
- Random background on load

How To Run
```
npm install
bower install
grunt serve
```

Access the site on http://localhost:9000/#/
